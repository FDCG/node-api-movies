const dotenv = require('dotenv')
dotenv.config()

const dbConfigs = {
    local: {
        client: 'mssql',
        connection: {

            user: 'FDCG1991',
            password: 'safari91',
            server: '127.0.0.1',
            host: '127.0.0.1',
            database: 'apiRestMovies'

        }
    },
    ort: {
        client: 'mssql',
        connection: {
            server: 'A-SRV-BDINST',
            host: 'A-SRV-BDINST',
            // A Definir
/*          user: 'BD21A01',
            password: 'BD21A01',
            database: 'BD21A01' */
        }
    }
}

const srvConfigs = {
    port: process.env.PORT || 4000,
    env: process.env.DB_ENV,
    mode: process.env.MODE,
    cases: process.env.CASES
}

module.exports = {
    dbConfig: dbConfigs[srvConfigs.env],
    port: srvConfigs.port,
    mode: srvConfigs.mode
}