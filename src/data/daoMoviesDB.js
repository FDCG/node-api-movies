const knex = require('../db/knex')

const Autolinker = require( 'autolinker' );

const url = 'localhost:4000/api/movies/?page='

async function getAll() {
    try {


        const selectAll = `SELECT * FROM Peliculas;`

        let result = await knex.raw(selectAll)

        if (result.length === 0) {
            throw { status: 404, description: "No hay peliculas" }
        }

        return trimAll(result)

    } catch (err) {

        throw err

    }
}



async function getByPage(page) {
    try {
        
 
        const result = await getAll()

        trimAll(result)

        let pageResult = {}

        const pageCount = Math.ceil(result.length / 10);

        if (page > pageCount) {

            throw { status: 404, description: "El número de página supera el tope: " + pageCount }
        }
        
        let prevPage = null
        let nextPage = null

        if (page === 1) {
            prevPage = null
            if (page !== pageCount) {
                nextPage = url + (page + 1)
            }
        }else{
            prevPage = url + (page - 1)
            if (page !== pageCount) {
                nextPage = url + (page + 1)
            }
        }

        pageResult = {
            "page": page,
            "pageCount": pageCount,
            "movies": result.slice(page * 10 - 10, page * 10),
            "previousPage": Autolinker.link(prevPage),
            "nextPage": Autolinker.link(nextPage)
        }

        return pageResult

    } catch (err) {
        if (err.status == 404 || err.status == 400) {
            throw err
        }
        throw { status: 500, description: err.message }
    }
}

async function getById(id) {

    if (isNaN(id)) {
        throw { status: 400, description: 'El paràmetro informado no es nùmerico: ' + id }
    }
    else if (id < 0) {
        throw { status: 400, description: 'El paràmetro informado no puede ser menor a cero: ' + id }
    }

    try {

        const selectById = `SELECT TOP 1 * FROM Peliculas WHERE id=${id};`
        const result = await knex.raw(selectById)
        
        if (result.length === 0) {
            throw { status: 404, description: 'Pelicula no encontrada con ese ID: ' + id }
        }

        trimAll(result)

        return result[0]

    } catch (err) {
        throw err
    }
}

 

async function addMovie(movie) {
  
    try {
       

        let insertionQuery = 'INSERT INTO Peliculas '
        insertionQuery += '(title, year, director, rating) '
        insertionQuery += `VALUES ('${movie.title.replace('\'', '')}', ${movie.year}, '${movie.director}', ${movie.rating})`
        await knex.raw(insertionQuery)
        trimAll(movie)
        return movie

    } catch (err) {
        throw err
    }
}

function replaceCaracters (movie){
 
        for (const key in movie) {
            if (movie.hasOwnProperty(key)) {
                
                if (isNaN(movie[key])) {
                    
                    movie[key] = movie[key].trim()
                }
                if (movie[key] == undefined) {
                    
                    movie[key] = 'Sin informacion'
                }
                movie[key].toString().replace("'", "")
               
            }
        }
    
    return movie
}


async function deleteById(id) {

    if (isNaN(id)) {
        throw { status: 400, description: 'El paràmetro informado no es nùmerico: ' + id }
    }

    else if (id < 0) {
        throw { status: 400, description: 'El paràmetro informado no puede ser menor a cero: ' + id }
    }

    try {
        await getById(id)
        const deleteByIdQuery = `DELETE FROM Peliculas WHERE id=${id}`
        return await knex.raw(deleteByIdQuery)

    } catch (err) {
        if (err.status !== 500) {
            throw err
        }else{
            throw { status: 500, description: err.message }
        }
        
    }
}

async function updateById(id, newMovie) {
    try {

        await getById(id)
        let updateByIdQuery = 'UPDATE Peliculas '
        updateByIdQuery += `SET title='${newMovie.title}', `
        updateByIdQuery += `year=${newMovie.year}, director='${newMovie.director}', rating=${newMovie.rating} `
        updateByIdQuery += `WHERE id=${id};`
        await knex.raw(updateByIdQuery)
        trimAll(newMovie)
        return newMovie
    } catch (err) {
        if (err.status == 404) {
            throw err
        }else{
            throw { status: 500, description: err.message }
        }

 
    }
}

function trimAll(results) {
    var i =0
    while (i < results.length) {
        for (const key in results[i]) {
            if (results[i].hasOwnProperty(key)) {
                
                if (isNaN(results[i][key])) {
                    
                    results[i][key] = results[i][key].trim()
                }
                if (results[i][key] == undefined) {
                    
                    results[i][key] = 'Sin informacion'
                }
            }
        }
        i++
    }

    return results
}

module.exports = {
    getAll,
    getById,
    getByPage,
    addMovie,
    deleteById,
    updateById
}
