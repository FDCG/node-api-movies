const knex = require('../db/knex')

async function getAll() {
    try {
        //const selectAll = `SELECT id, nick, nombre, apellido, email, fechaNac FROM Usuarios;`
        const selectAll = `SELECT * FROM Usuarios;`
        let result = await knex.raw(selectAll)
        if (result.length == 0) {
           throw { status: 404, description: "No hay usuarios" }
        }
        return trimAll(result)
    } catch (err) {

        throw { status: 500, description: err.message }

    }
}



async function getByNick(nick) {
    try {
        const selectByNick = `SELECT TOP 1 * FROM Usuarios WHERE nick='${nick}';`
        const result = await knex.raw(selectByNick)
        if (result.length === 0) {
            throw { status: 404, description: "No existe el nick" }
        }
        trimAll(result)
        return result[0]
    } catch (err) {
        throw err
    }
}

async function getById(id) {

    if (isNaN(id)) {
        throw { status: 400, description: 'El parámetro informado no es númerico: ' + id }
    }
    else if (id < 0) {
        throw { status: 400, description: 'El parámetro informado no puede ser menor a cero: ' + id }
    }

    try {

 
        const selectById = `SELECT * FROM Usuarios WHERE id=${id};`
        const result = await knex.raw(selectById)

        if (result.length === 0) {
            throw { status: 404, description: `No se encontró el ID ${id}` }
        }

        trimAll(result)
        return result[0]
    } catch (err) {
        throw err
    }
}




async function getByEmail(email) {


    try {
        const selectByEmail = `SELECT TOP 1 * FROM Usuarios WHERE email='${email}';`
        const result = await knex.raw(selectByEmail)
        if (result.length === 0) {
            throw { status: 404, description: "No existe el mail" }
        }
        trimAll(result)
        return result[0]
    } catch (err) {
        throw err
    }
}



async function addUser(user) {

    // SOLO USAR PARA Q-RECORD-VIEWS
/*     const _user = await getByEmail(user.email)

    if (_user != null) {
        throw { status: 400, description: "Ya existe un usuario con ese Email" }
    }
 */
    try {
        
        let insertionQuery = 'INSERT INTO Usuarios '
        insertionQuery += '(nick, contrasenia, nombre, apellido, email, fechaNac) '
        insertionQuery += `VALUES ('${user.nick}', '${user.contrasenia}', '${user.nombre}', '${user.apellido}', '${user.email}', '${parseDate(user.fechaNac)}')`
        await knex.raw(insertionQuery)
        trimAll(user)
        return user

    } catch (err) {
        throw err
    }
}

function parseDate(date){
    arrDate = date.split('-')
    return arrDate[2]+ "-" + arrDate[1] + "-" + arrDate[0]
}


async function deleteById(id) {

    try {
        await getById(id)
        const deleteByIdQuery = `DELETE FROM Usuarios WHERE id=${id}`
        return await knex.raw(deleteByIdQuery)

    } catch (err) {
        throw err
    }
}

async function updateById(id, user) {
    try {
        await getById(id)
        
        let updateByIdQuery = 'UPDATE Usuarios '
        updateByIdQuery += `SET nick='${user.nick}', `
        updateByIdQuery += `contrasenia='${user.contrasenia}', nombre='${user.nombre}', apellido='${user.apellido}', email='${user.email}', fechaNac='${user.fechaNac}' `
        updateByIdQuery += `WHERE id=${id};`
        await knex.raw(updateByIdQuery)
        trimAll(user)
        return user
    } catch (err) {
        if (err.status == 404) {
            throw err
        }else{
            throw { status: 500, description: err.message }
        } }
}

function trimAll(results) {
    var i = 0
    while (i <= results.length) {
        for (const key in results[i]) {
            if (results[i].hasOwnProperty(key)) {
                if (results[i][key] != null) {
                    row = results[i][key].toString()
                    if (row) {
                        results[i][key] = row.trim()
                    } else {
                        results[i][key] = row
                    }
                } else {
                    results[i][key] = null
                }

            }
        }
        i++
    }

    return results
}

module.exports = {
    getAll,
    getById,
    getByNick,
    addUser,
    deleteById,
    updateById,
    getByEmail
}
