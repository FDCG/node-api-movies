const fs = require('fs')
const Joi = require('@hapi/joi')

const validParameters = ['dir', 'format', 'name', 'sort', 'delimiter', 'headers']



async function exportWithParameters(data, parameters){
        
      for (const iterator in parameters.query) {
        var foundParam = validParameters.find(p => p == iterator )
        if (foundParam == null) {
            
        throw {status: 400, description: "Error Parametros: el nombre del parametros enviado es erroneo"}
            
        }
    }
        return exportFile(
            data,
            parameters.query.dir,
            parameters.query.name,
            parameters.query.format,
            parameters.query.sort,
            parameters.query.delimiter,
            parameters.query.headers
        )
}



/**
 * @param {Object} data Objeto a exportar
 * @param {string} directorio Ruta absoluta para exportar el archivo
 * @param {string} nombre Nombre del archivo
 * @param {string} formato Formato del archivo
 * @param {char} delimitador Caracter para delimitar los campos
 * @param {Object} usarKeysComoCabecera Si utiliza atributos del objeto como cabecera del archivo
 */
async function exportFile(datos, directorio, nombre, formato, orden, delimitador, usarKeysComoCabecera) {

    try {

        const data = await isValidData(datos, orden)

        const directory = await isValidDirectory(directorio)

        const name = await isValidName(nombre)

        const format = await isValidFormat(formato)

        const delimiter = await isValidDelimiter(delimitador)

        const headers = await withHeader(usarKeysComoCabecera)

        await generateWithFormat(directory, name, format, processData(data, delimiter, headers), true)

        return {status: "200" , 
                description: `Exportación exitosa. Nombre del Archivo: ${directory + name + format}` 
               
            }

    } catch (error) {
        throw error
    }
}

function getOrder(row, indexOrder) {

    const length = Object.keys(row).length

    if (parseInt(indexOrder) > length || parseInt(indexOrder) <= 0) {
        throw {status: 400, description: "Error Order: la propiedad informada esta fuera de los rangos"}
    }
    else if (isNaN(indexOrder) && indexOrder !== undefined) {
        throw {status: 400, description: "Error Order: la propiedad informada no es numérica"}
    } else if (indexOrder === "" || indexOrder === null || indexOrder === undefined) {
        return Object.keys(row)[0]
    }

    return Object.keys(row)[indexOrder - 1]

}

async function isValidData(data, order) {

    let finalData = []
    if (data == null || data == undefined) {
        throw {status: 404, description: `Error Datos: faltan datos para procesar`}
    }

    let i = 0
    while (i < data.length) {

        finalData = await combineArrays(finalData, data[i], order)
        i++
    }

    return finalData
}

function isValidDirectory(dir) {

    var rg1 = /^[a-zA-Z]:\/(\w+\/)*\w*$/

    if (!rg1.test(dir)) {
        const dirDefault = "C:/ApiRestMovies";
        if (!fs.existsSync(dirDefault)) {
            fs.mkdirSync("C:/ApiRestMovies")
        } else { return dirDefault + "/" }
    }
    if (!dir.endsWith('\/')) {
        return dir + '/'
    }
    return dir

}


function withHeader(use) {

    if (use === false || use == 'false') {
        return false
    } else { return true }

}

function isValidDelimiter(delimitador) {
    if (delimitador !== undefined && delimitador.length > 1) {
        throw {status: 400, description: "Error Delimitador: debe ser de 1(un) caracter"}
    } else if (delimitador == "" || delimitador == " " || delimitador == null) {
        return ';'
    } else if (!isNaN(delimitador)) {
        throw {status: 400, description: "Error Delimitador: no debe ser numérico"}
    } else { return delimitador }

}

function isValidFormat(format) {
    if (format === '.txt' || format === "txt") {
        return '.txt'
    } else if (format === '.csv' || format === 'csv') {
        return '.csv'
    } else if (format == null || format == "" || format === undefined) {
        return '.txt'
    } else {
        throw {status: 400, description: "Error Formato: inválido"}
    }
}

function isValidName(name) {

    let finalName = name

    if (name == "" || name == null) {
        finalName = 'Result_Export_' + getDate()
    } else if (invalidCharacters(name)) {
        throw {status: 400, description: "Error Nombre Archivo: el valor" +
        " informado contiene caracteres inválidos o no permitidos"}
    }

    return finalName

}


function invalidCharacters(text) {
    var rg1 = /^[^\\/:\*\?"<>\|]+$/; // Caracteres prohibidos \ / : * ? " < > |
    var rg2 = /^\./; // No puede comenzar con un punto
    var rg3 = /^(nul|prn|con|lpt[0-9]|com[0-9])(\.|$)/i; // Nombre de archivos prohibidos
    return !rg1.test(text) && !rg2.test(text) && !rg3.test(text);
}


function generateWithFormat(directory, name, format, data, shouldCreateIfNotExists) {
try {
    const path = directory + name + format
    let mode = 'r+'
    if (shouldCreateIfNotExists) {
        mode = 'w'
    }
    for (const obj of data) {
        fs.writeFileSync(path, data.join('\r\n'), { flag: mode })
    }
} catch (error) {
        throw {status: 500, description: error.message}
}
   
}

function getHeader(data, delimiter) {

    if (data == null || delimiter == null) {
        throw {status: 500, description: "Error al generar cabecera de archivo"}
    }

    let header = ""

    for (let key in data) {

        if (data.hasOwnProperty(key)) {
            header += key + delimiter
        }
    }

    return header
}

function getLine(data, delimiter) {

    if (data == null || data == undefined || data.length == 0) {
        throw {status: 500, description: "Error al procesar linea: Sin datos"}
    } else if (delimiter == null || delimiter == "" || delimiter == undefined) {
        throw {status: 500, description: "Error al procesar linea: Sin delimitador"}
    }

    let line = ""

    for (let key in data) {

        if (data.hasOwnProperty(key)) {
            line += data[key] + delimiter
        }
    }

    return line

}

function processData(data, delimiter, useHeader) {

    let arrayLines = []

    if (useHeader) {
        arrayLines.push(getHeader(data[0], delimiter).toUpperCase())
    }

    for (const obj of data) {
        arrayLines.push(getLine(obj, delimiter))
    }

    return arrayLines
}

function getDate() {

    let d = new Date

    let dateString = d.getFullYear() + '_' + d.getMonth() + '_' + d.getDay()

    return dateString

}

function dynamicSort(key) {

    return function (a, b) {
        return ((a[key] == b[key]) ? 0 : ((a[key] > b[key]) ? 1 : -1));
    }
}


function combineArrays(arrA, arrB, key) {

    const combinado = []

    let indexA = 0
    let indexB = 0

    //let i = 0;

    while (indexA < arrA.length || indexB < arrB.length) {
        if (indexA >= arrA.length) {
            //arrB[indexB].id = i
            combinado.push(arrB[indexB])
            indexB++
            //i++
        } else if (indexB >= arrB.length) {
            //arrA[indexA].id = i
            combinado.push(arrA[indexA])
            indexA++
            //i++
        } else if (arrA[indexA].title.localeCompare(arrB[indexB].title) == -1) {
            //arrA[indexA].id = i
            combinado.push(arrA[indexA])
            indexA++
            //i++
        } else if (arrB[indexB].title.localeCompare(arrA[indexA].title == -1)) {
            //arrB[indexB].id = i
            combinado.push(arrB[indexB])
            indexB++
            //i++
        } else {
            //arrA[indexA].id = i
            combinado.push(arrA[indexA])
            indexA++
            indexB++
            //i++
        }
    }

    if (combinado.length != 0) {
        property = getOrder(combinado[0], key)
        combinado.sort(dynamicSort(property));
    }


    return combinado
}

function isObjectDataValid(data) {
    const schema = {
        id: Joi.number().integer().min(1).max(99999999).required(),
        title: Joi.string().alphanum().min(1).required(),
        director: Joi.string().alphanum().min(1).required(),
        year: Joi.number().integer().min(1899).max(new Date().getFullYear()).required(),
        rating: Joi.number().integer().min(0).max(10).required()

    }
    const { error } = Joi.validate(data, schema);
    return error
}

module.exports = {
    exportFile,
    exportWithParameters
}
