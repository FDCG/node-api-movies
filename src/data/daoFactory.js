
const moviesDAO_array = require('./daoMoviesArray.js')
const moviesDAO_DB = require('./daoMoviesDB.js')


const usersDAO_array = require('./daoUsersArray.js')
const usersDAO_DB = require('./daoUsersDB.js')


const exportDAO = require('./daoExport.js')

const { mode } = require('../config')


function getMoviesDAO() {

    switch (mode) {
        case 'online': return moviesDAO_DB
        case 'offline': return moviesDAO_array
        default: throw "invalid mode. check system config!"
    }

}

function getUsersDAO() {

     switch (mode) {
        case 'online': return usersDAO_DB
        case 'offline': return usersDAO_array
        default: throw "invalid mode. check system config!"
    }

}

function getExportDAO(){
  return exportDAO

}



module.exports = {
    getMoviesDAO,
    getUsersDAO,
    getExportDAO

}

