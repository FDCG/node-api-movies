const movies = require('../sample.json')

const Autolinker = require('autolinker');

const url = 'localhost:4000/api/movies/?page='

async function getAll() {

    return movies

}

async function getById(id) {

    if (isNaN(id)) {
        throw { status: 400, description: 'El paràmetro informado no es nùmerico: ' + id }
    }
    else if (id < 0) {
        throw { status: 400, description: 'El paràmetro informado no puede ser menor a cero: ' + id }
    }

    const movie = await movies.find(m => m.id == id)

    if (movie == null) {
        throw { status: 404, description: 'Pelicula no encontrada con ese ID' }
    } else {

        return movie
    }

}

async function getByPage(page) {

    try {


        let pageResult = {}

        const pageCount = Math.ceil(movies.length / 10);

        if (page > pageCount) {

            throw { status: 404, description: "El número de página supera el tope: " + pageCount }
        }



        let prevPage = null
        let nextPage = null

        if (page === 1) {
            prevPage = null
            if (page !== pageCount) {
                nextPage = url + (page + 1)
            }
        }else{
            prevPage = url + (page - 1)
            if (page !== pageCount) {
                nextPage = url + (page + 1)
            }
        }


        pageResult = {
            "page": page,
            "pageCount": pageCount,
            "movies": movies.slice(page * 10 - 10, page * 10),
            "previousPage": Autolinker.link(prevPage),
            "nextPage": Autolinker.link(nextPage)
        }

        return pageResult

    } catch (err) {
        if (err.status == 404 || err.status == 400) {
            throw err
        }
        throw { status: 500, description: err.message }
    }
}




async function addMovie(newMovie) {

    // Esta comentado porque newMovie no tiene ID, entonces pincha ahi
    // Tambien cambio la forma de obtener el id porque la eliminar une pelicula puede cambiar el length haciendo que ponga un ID que ya exista

    /*   const movie = await getById(newMovie.id)
  
      if (movie != null) {
          throw { status: 400, description: "Ya existe una pelicula con ese ID" }
      }
  
   */
    []
    /* const id = movies.length + 1; */
    const id = movies[movies.length - 1].id + 1;
    newMovie = { id, ...newMovie }

    movies.push(newMovie)

    return newMovie

}

async function deleteById(id) {

    const movieIndex = await getById(id)


    movies.splice(movieIndex, 1)

    return movieIndex

}



async function updateById(id, newMovie) {

    const movieIndex = await movies.findIndex(m => m.id == id)

    if (movieIndex == -1) {
        throw { status: 404, description: "No existe una pelicula con ese ID" }

    }
    // Arreglo: con la nueva peli (sin ID), 
    // resetear un newMovie con ID + ...(lo que ya habia en newMovie)
    newMovie = { id, ...newMovie }
    movies.splice(movieIndex, 1, newMovie)

    return newMovie

}

module.exports = {

    getAll,
    getById,
    getByPage,
    addMovie,
    deleteById,
    updateById
}

