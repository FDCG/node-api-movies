const users = require('../users.json')

function getAll() {

    if (users.length == 0) {
      throw  { status: 404, description: "No hay usuarios" }
    }

    return users

}

async function getById(id) {

    if (isNaN(id)) {
         throw {status: 400 , description : 'El paràmetro informado no es nùmerico: ' + id}
     } 
    else if (id < 0) {
         throw {status: 400 , description : 'El paràmetro informado no puede ser menor a cero: ' + id}
     }
 
     const user = await users.find(u => u.id == id)
 
     if (user == null) {
        throw { status: 404, description: `No se encontró el ID ${id}` }
     }else{
 
     return user
     }
 
 }

 async function getByNick(nick) {

    
     const user = await users.find(u => u.nick == nick.trim())
 
     if (user == null) {
        throw {status: 404 , description : 'Usuario no encontrado con ese nick'}
     }else{
 
     return user
     }
 
 }

 async function getByEmail(email) {

    
    const user = await users.find(u => u.email == email.trim())

    if (user == null) {
       throw {status: 404 , description : 'Usuario no encontrado con ese mail'}
    }else{

    return user
    }

}

async function addUser(newUser) {

    const id = users[users.length-1].id + 1;
    newUser = { id, ...newUser }

    users.push(newUser)

    return newUser

    
}

async function deleteById(id) {

    const userIndex = await getById(id)
    


    users.splice(userIndex, 1)

    return userIndex

}

async function updateById(id, newUser) {

    const userIndex = await users.findIndex(u => u.id == id)

    if (userIndex == -1) {
        throw { status: 404, description: "No existe una usuario con ese ID" }

    }
  
    newUser = { id, ...newUser } 
    users.splice(userIndex, 1, newUser)

    return newUser

}




 module.exports = {
     getAll,
     getById,
     getByNick,
     getByEmail,
     addUser,
     deleteById,
     updateById
 }
 