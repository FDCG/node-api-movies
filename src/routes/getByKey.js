getByKeys(publicaciones, atributos)

function getByKeys(publicaciones, atributos) {

    let publicacionesFiltradas = []

    let cantidadAtributos = Object.keys(atributos).length

    for (const publicacion of publicaciones) {

        let contadorCoincidencias = 0

        // recorrer cada item de los atributos pasados

        var j = 0

        while (j < cantidadAtributos) {

            var i = 0 // para recorrer cada item de UNA publicacion

            while (i < Object.keys(publicacion).length) {
                // si coincide suma uno a las coincidencias

                // Por Ejemplo => si tenes un filtro con id: 4 y region: caba
                // evaluara que la publicacion tenga 'id' y que sea 4 y además
                // que tenga 'region' y que valga 'caba'
                if (Object.keys(atributos)[j] === Object.keys(publicacion)[i] &&
                    Object.values(atributos)[j] === Object.values(publicacion)[i] ) {
                            // si encuentro, adentro coincidencia
                    contadorCoincidencias++
                }
                i++
            }
            j++
        }

        // Si encontro la misma cantidad de coincidencias 
        //que la cantidad de atributos a buscar, significa que aplica
        
        if (contadorCoincidencias == cantidadAtributos) {
            publicacionesFiltradas.push(publicacion)
        }
    }

}