const { Router } = require('express');
const router = new Router();
const _ = require('underscore');
const daoFactory = require('../data/daoFactory')




router.get('/', (req, res) => {

    if (_.isEmpty(req.query)) {
        exportAll(req, res)
    } else {
        exportWithParams(req, res)
    }
})



async function exportAll(req, res) {

    try {
        let resultMovies = []
        const moviesDAO = daoFactory.getMoviesDAO()
        resultMovies.push(await moviesDAO.getAll())

        const exportDAO = daoFactory.getExportDAO()
        const result = await exportDAO.exportFile(resultMovies)
        res.json(result)

    } catch (error) {
        if  (isNaN(error.status)) {
            res.status(500).json("Error en el servidor")
        }else{
            res.status(error.status).json(error)
        }
        
    }

}

async function exportWithParams(req, res) {

    try {

        let resultMovies = []
        const moviesDAO = daoFactory.getMoviesDAO()
        resultMovies.push(await moviesDAO.getAll())
        
        const exportDAO = daoFactory.getExportDAO()

        const result = await exportDAO.exportWithParameters(resultMovies, req)
        res.json(result)

    } catch (error) {
        if  (isNaN(error.status)) {
            res.status(500).json("Error en el servidor")
        }else{
            res.status(error.status).json(error)
        }
        
    }

}

module.exports = router


