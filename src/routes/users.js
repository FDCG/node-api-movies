const { Router } = require('express');
const router = new Router();
const _ = require('underscore');
const daoFactory = require('../data/daoFactory')
const BaseJoi = require('@hapi/joi');
const Extension = require('@hapi/joi-date');
const Joi = BaseJoi.extend(Extension);




router.get('/', (req, res) => {

    if (_.isEmpty(req.query)) {
        getAllUsers(req, res)
    }
    else {

        let key = Object.keys(req.query)

        if (key.length > 1) {
            res.json({ status: 400, description: 'El formato del parametro enviado no es válido' })
        }
        else {

            switch (key[0]) {
                case 'id':
                    getUsersById(req, res)
                    break;

                case "nick":
                    // El parametro mandando es Case Sentitive
                    getUsersByNick(req, res)
                    break
                // El parametro mandando es Case Sentitive
                case "email":
                    getUsersByEmail(req, res)
                    break;

                default:
                    res.json({ status: 400, description: 'El parametro enviado no corresponde a una consulta válida' })
                    break;
            }
        }
    }

});



async function getUsersByNick(req, res) {

    try {
        let nick = req.query.nick
        const usersDAO = daoFactory.getUsersDAO()
        const result = await usersDAO.getByNick(nick)

        res.json(result)

    } catch (error) {
        res.status(error.status).json(error)

    }

};


async function getUsersById(req, res) {

    try {
        let id = req.query.id;
        const usersDAO = daoFactory.getUsersDAO()
        const result = await usersDAO.getById(id)

        res.json(result)

    } catch (error) {
        res.status(error.status).json(error)

    }

};

async function getUsersByEmail(req, res) {

    try {
        let email = req.query.email;
        console.log(email)
        const usersDAO = daoFactory.getUsersDAO()
        const result = await usersDAO.getByEmail(email)
        console.log(result)
        res.json(result)


    } catch (error) {
        res.status(error.status).json(error)

    }

};

async function getAllUsers(req, res) {

    try {
        const userDAO = daoFactory.getUsersDAO()
        const result = await userDAO.getAll()
        res.json(result)

    } catch (error) {

        res.status(error.status).json(error)
    }

};

router.post('/', async (req, res) => {


    try {
        const newUser = req.body
        const userDAO = daoFactory.getUsersDAO()
        let invalid = esUsuarioInvalido(newUser, false)
        if (invalid) {
            let details = invalid.details[0].message
            throw { status: 400, description: "Formato JSON inválido-", detail: details }

        }

        const result = await userDAO.addUser(newUser)

        res.status(201).json(result);

    } catch (error) {
       
        if (error.status != 500) {
            
            res.status(error.status).json(error)
        }else{

        res.status(500).json({ error: 'Se generó un error en el servidor' });
        }
    }


});





router.delete('/:id', async (req, res) => {


    try {

        const userDAO = daoFactory.getUsersDAO()
        const result = await userDAO.deleteById(req.params.id)

        res.json(result);

    } catch (error) {
        
        res.status(error.status).json(error)
    }


});

router.put('/:id', async (req, res) => {

    try {
        if (isNaN(req.params.id)) {
            throw { status: 400, description: 'El id informado no es numérico' }
        }

        const nuevoUser = req.body

        let invalid = esUsuarioInvalido(nuevoUser, true)
        if (invalid) {
            let details = invalid.details[0].message
            throw { status: 400, description: "Formato JSON inválido-", detail: details }

        }

        if (req.params.id != nuevoUser.id) {
            throw { status: 400, description: 'El id informado no coincide entre el recurso buscado y el nuevo' }
        }

        const userDAO = daoFactory.getUsersDAO()
        const userActualizado = await userDAO.updateById(req.params.id, nuevoUser)
        res.json(userActualizado)
    } catch (err) {
        
        res.status(err.status).json(err)
    }
})

function esUsuarioInvalido(usuario, conId) {

    let schema = {

        nick: Joi.string().min(1).max(20).required(),
        contrasenia: Joi.string().min(1).max(20).required(),
        nombre: Joi.string().min(1).max(50).required(),
        apellido: Joi.string().min(1).max(50).required(),
        email: Joi.string().email().min(1).max(50).required(),
        fechaNac: Joi.date().format('DD-MM-YYYY').required()
    }

    if (conId) {
        schema = {id: Joi.number().integer().min(1).max(9999999).required(), ...schema}
    }
    const { error } = Joi.validate(usuario, schema);
    
    return error
}


module.exports = router; 