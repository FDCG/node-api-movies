const { Router } = require('express');
const router = new Router();
const _ = require('underscore');
const daoFactory = require('../data/daoFactory')
const Joi = require('@hapi/joi')


async function getMoviesById(req, res) {

    try {
        let id = req.query.id;
        const moviesDAO = daoFactory.getMoviesDAO()
        const result = await moviesDAO.getById(id)

        res.json(result)


    } catch (error) {
        
        res.status(error.status).json(error)
    }

};

router.get('/', (req, res) => {

    if (_.isEmpty(req.query)) {
        getAllMovies(req, res)
    } else {
        if (req.query['id']) {
            getMoviesById(req, res)
        }
        else if (req.query['page']) {
            getMoviesByQuery(req, res)
        }
        else {
            throw { status: 400, description: 'El parametro enviado no corresponde a una consulta válida' }
        }

    }
})

async function getMoviesByQuery(req, res) {


    try {

        let page = parseInt(req.query.page);
        
        if (isNaN(page)) {
            throw { status: 400, description: "El parámetro ingresado no es numérico " }
        }

        if (page <= 0) {
            throw { status: 400, description: "El parámetro ingresado no puede ser 0 o menor a cero " }
        }

      
    
        const moviesDAO = daoFactory.getMoviesDAO()
        const result = await moviesDAO.getByPage(page)
        
        res.json(result);



    } catch (error) {
       
        
        res.status(error.status).json(error)
    }


};

async function getAllMovies(req, res) {

    try {
        const moviesDAO = daoFactory.getMoviesDAO()
        const result = await moviesDAO.getAll()
        res.json([{title:"saraza"}])// result)


    } catch (error) {

        res.status(error.status).json(error)
    }

};


router.post('/', async (req, res) => {


    try {
        const newMovie = req.body;
        const moviesDAO = daoFactory.getMoviesDAO()

        if (esPeliculaInvalida(newMovie)) {
            throw { status: 400, description: "Error Pelicula: los datos no corresponden a una pelicula valida" }
        }

        const result = await moviesDAO.addMovie(newMovie)

        res.status(201).json(result);

    } catch (error) {
        
        res.status(error.status).json(error)
    }


});



router.put('/:id', async (req, res) => {


    try {

        if (isNaN(req.params.id))
            throw { status: 400, description: 'Error: El id informado no es numérico' }

            if (req.params.id<0)
            throw { status: 400, description: 'Error: El id informado no puede ser menor a cero' }

            const nuevaPeli = req.body

         if (esPeliculaConIdInvalida(nuevaPeli))
            throw { status: 406, description: 'Error: La pelicula posee un formato json invalido o faltan datos' }
 
        if (req.params.id != nuevaPeli.id)
            throw { status: 400, description: 'Error: El id informado no coincide entre el recurso buscado y el nuevo' }

        const moviesDAO = daoFactory.getMoviesDAO()
        const peliActualizada = await moviesDAO.updateById(req.params.id, nuevaPeli)
        res.status(201).json(peliActualizada)
    } catch (error) {
       
        res.status(error.status).json(error)
    }
})


router.delete('/:id', async (req, res) => {


    try {

        const moviesDAO = daoFactory.getMoviesDAO()
        const result = await moviesDAO.deleteById(req.params.id)

        
        res.json(result);

    } catch (error) {
        res.status(error.status).json(error)
    }


});

function esPeliculaInvalida(pelicula) {
    const schema = {
        title: Joi.string().min(1).required(),
        director: Joi.string().min(1).required(),
        year: Joi.number().integer().min(0).max(2019).required(),
        rating: Joi.number().min(0).max(10).required()
    }
    const { error } = Joi.validate(pelicula, schema);
    
    return error
}

function esPeliculaConIdInvalida(pelicula) {
    const schema = {
        id: Joi.number().integer().min(1).max(9999999).required(),
        title: Joi.string().min(1).required(),
        director: Joi.string().min(1).required(),
        year: Joi.number().integer().min(0).max(2019).required(),
        rating: Joi.number().min(0).max(10).required()
    }
    const { error } = Joi.validate(pelicula, schema);

    return error
}


module.exports = router

