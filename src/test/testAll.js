// Test Movies

const testGet = require('./movies/testGet');
const testGetById = require('./movies/testGetById');
const testGetByPage = require('./movies/testGetByPage');
const testAddNewMovie = require('./movies/testAddNewMovie');
const testPutById = require('./movies/testPutById');
const testDeleteById = require('./movies/testDeleteById');

// Test Users

const testGetUsers = require('./users/testGetUsers');
const testGetUsersById = require('./users/testGetUsersById');
const testAddNewUser = require('./users/testAddNewUser');
const testPutUserById = require('./users/testPutUserById');
const testDeleteUserById = require('./users/testDeleteUserById');


// Test Export
const testExportAll = require('./export/testExportAll');
const testGetWithParams = require('./export/testExportAllWithParameters');

// Color

const url = 'http://localhost:4000/api/'



async function main() {

    const colors = require('colors');

    console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'.bgGreen)
    console.log('%%%%%                   INICIANDO TEST FUNCIONES                             %%%%'.bgGreen)
    console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'.bgGreen)

    let allEndPointResult = []

    //MOVIES
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGet(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGetById(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGetByPage(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testAddNewMovie(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testPutById(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testDeleteById(url))


    // USERS
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGetUsers(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGetUsersById(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testAddNewUser(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testPutUserById(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testDeleteUserById(url))

    // EXPORT
    allEndPointResult = await addResultsToArray(allEndPointResult, await testExportAll(url))
    allEndPointResult = await addResultsToArray(allEndPointResult, await testGetWithParams(url))

 
      


    let errors = []

    for (let index = 0; index < allEndPointResult.length; index++) {
        if (allEndPointResult[index]["proof"] == 'FAIL') {
            let endPoint = allEndPointResult[index]
            errors.push(await getEndPointError(endPoint))
        }

    }

    console.log("-----Resultado de los Test-----\n")
    console.log(`Pruebas Realizadas: ${allEndPointResult.length}`)
    console.log(`Pruebas Exitosas: ${(allEndPointResult.length - errors.length)}`)

    if (errors.length > 0) {
        console.log("\n-----Errores-----")

        for (let j = 0; j < errors.length; j++) {
            console.log(`Endpoint............: ${errors[j].uri}`)
            console.log(`Prueba..............: ${errors[j].proof}`)
            console.log(`Resultado esperado..: ${errors[j].expected}`)
            console.log(`Resultado obtenido..: ${errors[j].gotten}`)
            console.log(`---------------------`)
        }
    }

}

async function getEndPointError(obj) {

    let endPoint = {}
    let opts = obj.res.options
    endPoint.uri = `${opts.method}: ${opts._uri}`
    endPoint.proof = obj.description
    endPoint.expected = `STATUS ${obj.esp}`
    endPoint.gotten = `STATUS ${obj.res.error.status} ${obj.res.error.description}`

    return endPoint
}


async function addResultsToArray(resArray, resObject) {
    let resultArray = resArray
    for (let i = 0; i < resObject.length; i++) {
        resultArray.push(resObject[i])
    }
    return resultArray
}

main()