const request = require('request-promise-native');

//testExportAll('http://localhost:4000/api/')

async function testExportAll(url) {
    
    let result = []

    const options = {
        method: 'GET',
        _uri: 'movies/export/',
        uri: url + 'movies/export/',
        json: true
    }

    try {
        const _exports = await request(options)
        result.push({ proof: 'OK', description: "Export All Ok", res: options })
    } catch (err) {
        result.push({proof: 'FAIL', description: "OK", esp: 200, res: err, params: null})
    }
    //console.log(result)
    return result
}


module.exports = testExportAll