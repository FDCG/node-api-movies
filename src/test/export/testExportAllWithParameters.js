const request = require('request-promise-native');
//testGetWithParams('http://localhost:4000/api/')

async function testGetWithParams(url){

    let testResults = []
    // Con ruta erronea con salida OK
    testResults.push(await runTest(url,'sdbasdbfr', 'Prueba1', '.txt', 1, ';', true, 200, "OK"))
    // Con nombre erroneo con salida Error
    testResults.push(await runTest(url,'c:/ApiRestMovies', '**/-??Malescrito', '.txt', 1, ';', true, 400, "Con error en nombre"))
    // Con formato erroneo
    testResults.push(await runTest(url,'c:/ApiRestMovies', 'Prueba3', '-pdf', 1, ';', true, 400, "Con error en formato"))
    // Con ordenamiento erroneo
    testResults.push(await runTest(url,'c:/ApiRestMovies', 'Prueba4bis', '.txt', -1, ';', true, 400, "Con ordenamiento erroneo"))
    // Con ordenamiento erroneo
    testResults.push(await runTest(url,'c:/ApiRestMovies', 'Prueba4', '.txt', 'A', ';', true, 400, "Con parametro no numerico"))
    // Con delimitador erroneo
    testResults.push(await runTest(url,'c:/ApiRestMovies', 'Prueba5', '.txt', 1, '=?', true, 400, "Con error en delimitador"))

    return testResults
}

/**
 * @param {string} url Uri de la peticion
 * @param {string} dir Ubicación directorio a exportar archivo
 * @param {string} name Nombre del archivo
 * @param {string} format formato del archivo
 * @param {string} sort Orden del archivo segun columna
 * @param {char} delimiter Caracter para delimitar los campos
 * @param {Object} headers Si utiliza atributos del objeto como cabecera del archivo
 */
async function runTest(url, dir, name, format, sort, delimiter, headers, errCode, msg) {
    
    let result = null
    

    const options = {
        method: 'GET',
        _uri: 'movies/export/{dir, name, format, sort, delimiter, headers}',
        uri: url + 'movies/export/',
        qs: {
            dir: dir,
            name: name,
            format: format,
            sort: sort,
            delimiter: delimiter,
            headers: headers
        },
        json: true
    }

    try {
        const res = await request(options)

        if (res.status != errCode) {
            result= { proof: 'FAIL', description: msg, esp: errCode,res: {options, error: {status:res.status, description: res.description}} }
        }else{
            result= { proof: 'OK', description: "Get Export With Parameters Ok", res: options }
        }

       

    } catch (err) {

        if (err.statusCode == errCode) {
            result={proof: 'OK', description: "Get EWP con error esperado", res: err }
        }
       else{
        result={ proof: 'FAIL', description: msg, esp: errCode, res: err }
       }
    }
    

    return result
}


module.exports = testGetWithParams