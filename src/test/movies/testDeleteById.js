const request = require('request-promise-native')



//testDeleteById('http://localhost:4000/api/')

async function testDeleteById(url) {

    let testResults = []
   
    testResults.push(await runTest(url, 2, 200, "OK")) 
    testResults.push(await runTest(url, -9, 400, "Parametro menor a cero"))
    testResults.push(await runTest(url, 99, 404, "Parametro con id inexistente"))

    return testResults
}

   
async function runTest(url, _id, errCode, msg) {

    let result = null

    const options = {

        method: 'DELETE',
        _uri: 'movies/id',
        uri: url + 'movies/' + _id,
        json: true
    }

    try {

        var res = await request(options)

        if (errCode != 200) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Delete Ok' } } }
        } else {
            result = { proof: 'OK', description: "Delete Ok", res: res }
        }

    } catch (err) {

        if (err.statusCode == errCode) {
            result={proof: 'OK', description: "Delete con error esperado", res: err}
        }
       else{
        result={ proof: 'FAIL', description: msg, esp: errCode, res: err }
       }
    }
    

    return result
}

module.exports = testDeleteById