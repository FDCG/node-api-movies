const request = require('request-promise-native');


//testAddNewUser('http://localhost:4000/api/')


async function testAddNewMovie(url) {

    let testResults = []

    const movieTest1 = {
        title: 'Pelicula Bien',
        year: 1989,
        director: 'Pepo',
        rating: 3.0
    }
    testResults.push(await runTest(url, movieTest1, 201, "CREATED"))

    const movieTest2 = {
        title: 'Otra Pelicula Bien',
        //year: 1999,
        director: 'Peponeio',
        rating: 4.5
    }
    testResults.push(await runTest(url, movieTest2, 400, "Body con datos faltantes"))

    return testResults

}

async function runTest(url, movie, errCode, msg) {

    let result = null;



    const options = {
        method: 'POST',
        _uri: 'movies/',
        uri: url + 'movies/',
        body: movie,
        json: true
    }

    try {

        var res = await request(options)

        if (errCode != 201) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 201, description: 'Post Ok' } } }
        } else {
            result = { proof: 'OK', description: "Post Ok", res: res }
        }

    } catch (err) {

        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Post con error esperado", res: err }

        } else {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: err, params: null }
        }

    }


    return result


}

module.exports = testAddNewMovie
