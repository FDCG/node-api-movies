const request = require('request-promise-native')

/* testGetMoviesById('http://localhost:4000/api/') */

async function testGetByPage(url) {


    let testResults = []
    testResults.push(await runTest(url, 1, 200, "OK"))
    testResults.push(await runTest(url, -1, 400, "Con Parametro menor a cero"))
    testResults.push(await runTest(url, 25, 404, "Con parametro mayor al rango vàlido")) //correcto 404
    testResults.push(await runTest(url, 'AB', 400, "Con Parametro no numerico"))

    return testResults
}



async function runTest(url, page, errCode, msg) {



    let result = null

    const options = {
        method: 'GET',
        _uri: 'movies/page',
        uri: url + 'movies/',
        qs: { page: page },
        json: true
    }

    try {

        var res = await request(options)

        if (!res.hasOwnProperty('page')) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get By page: Sin page' } } }
        }

        else if (!res.hasOwnProperty('pageCount')) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get By page: Sin pagecount' } } }
        }
        else if (!res.hasOwnProperty('movies')) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get By page: Sin movies' } } }
        }
        else if (!res.hasOwnProperty('previousPage')) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get By page: Sin previouspage' } } }
        }
        else if (!res.hasOwnProperty('nextPage')) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get By page: Sin nextPage' } } }
        } else {
            if (errCode != 200) {
                result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Get By Page Ok' } } }
            } else {
                result = { proof: 'OK', description: "Get By Page Ok", res: res }
            }
        }



    } catch (err) {

        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Get By Page con error esperado", res: err }
        }
        else {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: err }
        }
    }


    return result
}

module.exports = testGetByPage