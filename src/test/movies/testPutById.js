
const request = require('request-promise-native');

async function runTest(url, testMovie, id, errCode, msg) {

    let result = null

    const options = {

        method: 'PUT',
        _uri: 'movies/id',
        uri: url + 'movies/' + id,
        body: testMovie,
        json: true
    };

    try {

        const res = await request(options)

        if (errCode != 200) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Put Ok' } } }
        } else {
            result = { proof: 'OK', description: "Put Ok", res: res }
        }


    } catch (err) {
        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Put con error esperado", res: err }
        }
        else {
            result={ proof: 'FAIL', description: msg, esp: errCode, res: err, params: 'id' }
        }



    }
    
    return result
}

 

async function testPutById(url) {

    let testResult = []

    const newMovie1 = {
        id: 1,
        title: 'Pelicula en Put',
        director: 'Cambiamos',
        year: 1999,
        rating: 8.5
    }
    testResult.push(await runTest(url, newMovie1, 1, 200, 'OK'))

    const newMovie2 = {
        id: 2,
        title: 'Put en Pelicula',
        director: 'No cambiemos',
        year: 1997,
        rating: 8
    }
    testResult.push(await runTest(url, newMovie2, 9, 400, 'El id del recurso y el enviado no coinciden'))

  

    const newMovie3 = {
        id: 3,
        title: 'Nosotros los monos',
        director: 'Moncho',
        // year: 1999,
        rating: 9
    }
    testResult.push(await runTest(url, newMovie3, 3, 406, 'La película posee un formato json invalido o faltan datos'))


    const newMovie4 = {
        id: 125,
        title: 'No existe ID',
        director: 'Legrand',
        year: 1999,
        rating: 6.5
    }
    testResult.push(await runTest(url, newMovie4, 125, 404, 'Recurso no encontrado'))


    const newMovie5 = {
        id: -3,
        title: 'Es negativo, lo sè',
        director: 'Legrand',
        year: 1999,
        rating: 6.5
    }
    testResult.push(await runTest(url, newMovie5, -3, 400, 'Parámetro informado no es numérico o es menor a cero'))

    return testResult
  
}





module.exports = testPutById