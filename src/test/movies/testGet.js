const request = require('request-promise-native');

//testGet('http://localhost:4000/api/')

async function testGet(url) {

    let errCode = 200
    let msg = "Con respuesta ok"

    let result = []


    const options = {
        method: 'GET',
        _uri: 'movies/',
        uri: url + 'movies',
        json: true
    }

    try {
        const movies = await request(options)

        let testOk = true

        if (movies.length != undefined ) {

            for (i = 0; i < movies.length && testOk; i++) {


                if (!movies[i].hasOwnProperty('id')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get Movies: Sin id' } } })
                    testOk = false
                }
                else if (!movies[i].hasOwnProperty('year')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get Movies: Sin year' } } })
                    testOk = false
                }
                else if (!movies[i].hasOwnProperty('director')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get Movies: Sin director' } } })
                    testOk = false
                }

                else if (!movies[i].hasOwnProperty('rating')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get Movies: Sin rating' } } })
                    testOk = false
                }

                else if (!movies[i].hasOwnProperty('title')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get Movies: Sin title' } } })
                    testOk = false
                }


            }
        }
        else {
            testOk = false
            result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: json con formato invalido' } } })

        }

        if (testOk) {
            result.push({ proof: 'OK', description: "Get All Ok", res: options })

        }


    } catch (err) {

        result.push({ proof: 'FAIL', description: "OK", esp: 200, res: err, params: null })

    }


    return result
}


module.exports = testGet