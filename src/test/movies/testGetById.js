const request = require('request-promise-native')

/* testGetMoviesById('http://localhost:4000/api/') */

async function testGetById(url) {

    let testResults = []
    testResults.push(await runTest(url, 1, 200, "OK"))
    testResults.push(await runTest(url, 67, 404, "Parametro Id Inexistente"))
    testResults.push(await runTest(url, -1, 400, "Parametro menor a cero"))
    testResults.push(await runTest(url, 'AB', 400, "Parametro no numerico"))

    return testResults
}




async function runTest(url, _id, errCode, msg) {

    let result = null

    const options = {
        method: 'GET',
        _uri: 'movies/id',
        uri: url + 'movies' + '/',
        qs: { id: _id },
        json: true
    }

    try {

        let res = await request(options)

        if (!res.hasOwnProperty('id'))
            result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin Id' } } }
        else if (!res.hasOwnProperty('year'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin year' } } }
       
        else if (!res.hasOwnProperty('director'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin director' } } }
       
        else if (!res.hasOwnProperty('title'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin title' } } }
       
        else if (!res.hasOwnProperty('rating'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin rating' } } }
       
        else {
            if (errCode != 200) {
                result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Get By Id OK' } } }
            } else {
                result = { proof: 'OK', description: "Get By Id Ok", res: res }
            }
        }


    } catch (err) {
        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Get by Id con error esperado", res: err }
        }
        else {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: err, params: 'id' }

        }

    }


    return result
}

module.exports = testGetById