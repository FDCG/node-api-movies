const request = require('request-promise-native');


//testAddNewUser('http://localhost:4000/api/')


async function testAddNewUser(url) {

    let testResults = []

    const userTest1 = {

        nick: "FacuDany1991",
        contrasenia: "claveEscondida",
        nombre: "Facundo D",
        apellido: "Cer-Gom",
        email: "fdcg@hotmail.com",
        fechaNac: '13-07-1991'
    }

    testResults.push(await runTest(url, userTest1, 201, "CREATED"))

    const userTest2 = {

        //nick: "Gris1999",
        contrasenia: "12345",
        nombre: "Gris",
        //apellido: "MeDaIgual",
        email: "griskpa@hotmail.com",
        fechaNac: "27-07-1999"
    }

    testResults.push(await runTest(url, userTest2, 400, "Body con datos faltantes"))


    return testResults
}


async function runTest(url, user, errCode,msg) {

    let result=null;

    const options = {
        method: 'POST',
        _uri: 'users/',
        uri: url + 'users/',

        body: user,
        json: true
    }

    try {

        var res = await request(options)

        if (errCode != 201) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 201, description: 'Post Ok' } } }
        } else {
            result = { proof: 'OK', description: "Post Ok", res: res }
        }

    } catch (err) {

        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Post con error esperado", res: err }

        } else {
            result={ proof: 'FAIL', description: msg, esp: errCode, res: err, params: null }
        }

    }

    
    return result
}


module.exports = testAddNewUser
