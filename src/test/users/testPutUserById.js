
const request = require('request-promise-native');

//testPutUserById("http://localhost:4000/api/")



async function testPutUserById(url) {

    let testResult = []

    const testUsers1 = {
        id: 1,
        nick: "Gris1999",
        contrasenia: "12345",
        nombre: "Gris",
        apellido: "MeDaIgual",
        email: "griskpa@hotmail.com",
        fechaNac: "27-07-1999"
    }

    testResult.push(await runTest(url, testUsers1, 1, 200, 'OK'))

    const testUsers2 = {
        id: 3,
        nick: "Gris1999",
        contrasenia: "12345",
        nombre: "Gris",
        apellido: "MeDaIgual",
        email: "griskpa@hotmail.com",
        fechaNac: "27-07-1999"
    }

    testResult.push(await runTest(url, testUsers2, 9, 400, 'El id del recurso y el enviado no coinciden'))

    return testResult
}


async function runTest(url, testUser, id, errCode, msg) {

    let result = null


    const options = {

        method: 'PUT',
        _uri: 'users/id',
        uri: url + 'users/' + id,
        body: testUser,
        json: true
    };

    try {

        const res = await request(options)

        if (errCode != 200) {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Put Ok' } } }
        } else {
            result = { proof: 'OK', description: "Put Ok", res: res }
        }


    } catch (err) {
        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Put con error esperado", res: err }
        }
        else {
            result={ proof: 'FAIL', description: msg, esp: errCode, res: err, params: 'id' }
        }



    }
    
    return result
}

module.exports = testPutUserById