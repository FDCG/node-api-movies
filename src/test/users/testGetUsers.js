const request = require('request-promise-native');


testGetUsers('http://localhost:4000/api/')

async function testGetUsers(url) {

    let errCode = 200
    let msg = "Con respuesta ok"

    let result = []
    const options = {
        method: 'GET',
        _uri: 'users/',
        uri: url + 'users',
        json: true
    }

    try {
        const users = await request(options)

        let testOk = true

        if (users.length != undefined) {


            for (i = 0; i < users.length && testOk; i++) {


                if (!users[i].hasOwnProperty('id')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin id' } } })
                    testOk = false
                }
                else if (!users[i].hasOwnProperty('nick')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin nick' } } })
                    testOk = false
                }
                else if (!users[i].hasOwnProperty('contrasenia')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin contrasenia' } } })
                    testOk = false
                }

                else if (!users[i].hasOwnProperty('fechaNac')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin fechaNac' } } })
                    testOk = false
                }

                else if (!users[i].hasOwnProperty('nombre')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin nombre' } } })
                    testOk = false
                }

                else if (!users[i].hasOwnProperty('apellido')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin apellido' } } })
                    testOk = false
                }

                else if (!users[i].hasOwnProperty('email')) {
                    result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: Sin email' } } })
                    testOk = false
                }
            }
        }
        else {
            testOk=false
            result.push({ proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 400, description: 'Get All: json con formato invalido' } } })

        }

        if (testOk) {
            result.push({ proof: 'OK', description: "Get All Ok", res: options })

        }


    } catch (err) {

        result.push({ proof: 'FAIL', description: "OK", esp: 200, res: err, params: null })

    }


    return result

}


module.exports = testGetUsers