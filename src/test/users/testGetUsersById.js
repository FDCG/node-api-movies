const request = require('request-promise-native')

//testGetUsersById('http://localhost:4000/api/')

async function testGetUsersById(url) {

    let testResults = []
    testResults.push(await runTest(url, 1, 200, "OK"))
    testResults.push(await runTest(url, 67, 404, "Parametro Id Inexistente"))
    testResults.push(await runTest(url, -1, 400, "Parametro menor a cero"))
    testResults.push(await runTest(url, 'AB', 400, "Parametro no numerico"))

    return testResults
}


async function runTest(url, id, errCode, msg) {



    let result = null

    const options = {
        method: 'GET',
        _uri: 'users/id',
        uri: url + 'users/',
        qs: { id: id },
        json: true
    }
    try {

        let res = await request(options)

        if (!res.hasOwnProperty('id'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin Id' } } }
        else if (!res.hasOwnProperty('nick'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin nick' } } }
       
        else if (!res.hasOwnProperty('contrasenia'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin contrasenia' } } }
       
        else if (!res.hasOwnProperty('fechaNac'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin fechaNac' } } }
       
        else if (!res.hasOwnProperty('nombre'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin nombre' } } }
       
        else if (!res.hasOwnProperty('apellido'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin apellido' } } }
       
        else if (!res.hasOwnProperty('email'))
        result = { proof: 'FAIL', description: msg, esp: errCode, res:{ options, error: { status: 400, description: 'Get By id: Sin email' } } }
        else {
            if (errCode != 200) {
                result = { proof: 'FAIL', description: msg, esp: errCode, res: { options, error: { status: 200, description: 'Get By Id OK' } } }
            } else {
                result = { proof: 'OK', description: "Get By Id Ok", res: res }
            }
        }


    } catch (err) {
        if (err.statusCode == errCode) {
            result = { proof: 'OK', description: "Get by Id con error esperado", res: err }
        }
        else {
            result = { proof: 'FAIL', description: msg, esp: errCode, res: err, params: 'id' }

        }

    }


    return result

}

module.exports = testGetUsersById